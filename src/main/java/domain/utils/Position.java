package domain.utils;

import domain.enums.DirectionEnum;
import domain.service.MapService;

public class Position {

	public int x;
	public int y;

	public Position(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public Position(Position position) {
		this.x = position.x;
		this.y = position.y;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Position))
			return false;

		Position position = (Position) o;

		if (x != position.x)
			return false;
		return !(y != position.y);
	}

	@Override
	public int hashCode() {
		int result = x;
		result = 31 * result + y;
		return result;
	}

	@Override
	public String toString() {
		return "Position{" + "x=" + x + ", y=" + y + '}';
	}

	public Position nextTo(DirectionEnum direction) {
		int x = this.x;
		int y = this.y;
		switch (direction) {
		case RIGHT:
			x++;
			break;
		case LEFT:
			x--;
			break;
		case UP:
			y++;
			break;
		case DOWN:
			y--;
			break;
		default:
			throw new RuntimeException("Can't process direction: " + direction);
		}
		return x >= 0 && x < MapService.MAX_X && y >= 0 && y < MapService.MAX_Y ? new Position(x, y)
				: new Position(this.x, this.y);
	}
}
