package domain.service.actions;

import java.util.Date;

import domain.player.Player;

public class RangeAttack extends AbstractAction {

	public int targetX;
	public int targetY;

	public RangeAttack(int targetX, int targetY) {
		this.targetX = targetX;
		this.targetY = targetY;
	}

	@Override
	public Long getInterval() {
		return 100L;
	}

	@Override
	public long getLastAttempt(Player player) {
		return player.lastAttackTime;
	}

	@Override
	public ActionType getType() {
		return ActionType.RANGE_ATTACK;
	}

	@Override
	public void refreshUserInterval(Player player) {
		player.lastAttackTime = new Date().getTime();
	}

}
