package domain.service.actions;

import java.util.Date;

import domain.enums.DirectionEnum;
import domain.player.Player;

public class Movement extends AbstractAction {

	public DirectionEnum direction;

	public Movement(DirectionEnum direction) {
		this.direction = direction;
	}

	@Override
	public Long getInterval() {
		return 80L;
	}

	@Override
	public long getLastAttempt(Player player) {
		return player.lastMovementTime;
	}

	@Override
	public ActionType getType() {
		return ActionType.MOVEMENT;
	}
	
	@Override
	public void refreshUserInterval(Player player) {
		player.lastMovementTime = new Date().getTime();
	}

}
