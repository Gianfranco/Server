package domain.service.actions;

import java.util.Date;

import domain.player.Player;

public abstract class AbstractAction {

	public abstract Long getInterval();

	public abstract long getLastAttempt(Player player);

	public abstract ActionType getType();

	/**
	 * TODO: Esta decisicionn podria llegar a ser del usuario, tanto verificar si puede
	 * accionar como refrescar el intervalo. Para debatir.
	 */

	public boolean isAvailable(Player player) {
		return new Date().getTime() - this.getLastAttempt(player) >= this.getInterval();
	}

	public abstract void refreshUserInterval(Player player);

}
