package domain.service.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import domain.player.Player;
import domain.service.actions.executors.AttackExecutor;
import domain.service.actions.executors.MovementExecutor;

public class ActionManager {

	private static final Logger LOGGER = LoggerFactory.getLogger(ActionManager.class);
	private static ActionManager instance = null;

	public static ActionManager getInstance() {
		if (instance == null) {
			instance = new ActionManager();
		}
		return instance;
	}

	private AttackExecutor attackExecutor;
	private MovementExecutor movementExecutor;

	private ActionManager() {
		attackExecutor = AttackExecutor.getInstance();
		movementExecutor = MovementExecutor.getInstance();
	}

	// public void process(ActionDTO actionDTO, Player player) {
	// ActionEnum action = ActionEnum.parse(actionDTO.getAction());
	// DirectionEnum direction = DirectionEnum.parse(actionDTO.getDirection());
	// switch (action) {
	// case MOVE:
	// LOGGER.info("Action processed: MOVE");
	// this.movementExecutor.execute(player, direction);
	// break;
	// case ATTACK:
	// LOGGER.info("Action processed: ATTACK");
	// this.attackExecutor.execute(player);
	// break;
	// default:
	// LOGGER.error("Error al intentar procesar accion: " + action);
	// break;
	// }
	// }

	public void process(AbstractAction action, Player player) {
		LOGGER.info("Processing action={}", action.getType().name());
		if (action.getType().equals(ActionType.MOVEMENT)) {
			this.movementExecutor.tryExecute(action, player);
		} else {
			this.attackExecutor.tryExecute(action, player);
		}
	}
}
