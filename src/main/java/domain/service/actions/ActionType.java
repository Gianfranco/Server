package domain.service.actions;

import com.google.common.collect.Lists;

public enum ActionType {
	MOVEMENT(1), MELEE_ATTACK(2), RANGE_ATTACK(3);

	private Integer index;

	ActionType(Integer index) {
		this.index = index;
	}

	public Integer getIndex() {
		return index;
	}

	public static ActionType parse(Integer index) {
		return Lists.newArrayList(ActionType.values()).stream().filter(actionEnum -> actionEnum.index.equals(index))
				.findFirst().get();
	}
}
