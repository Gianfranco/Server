package domain.service.actions;

import java.util.Date;

import domain.player.Player;

public class MeleeAttack extends AbstractAction {

	@Override
	public Long getInterval() {
		return 100L;
	}

	@Override
	public long getLastAttempt(Player player) {
		return player.lastAttackTime;
	}

	@Override
	public ActionType getType() {
		return ActionType.MELEE_ATTACK;
	}

	@Override
	public void refreshUserInterval(Player player) {
		player.lastAttackTime = new Date().getTime();
	}

}
