package domain.service.actions.executors;

import org.slf4j.Logger;

import domain.player.Player;
import domain.service.MapService;
import domain.service.PlayerService;
import domain.service.actions.AbstractAction;

public abstract class AbstractActionExecutor {

	protected MapService mapService;
	protected PlayerService playerService;

	public AbstractActionExecutor() {
		this.mapService = MapService.getInstance();
		this.playerService = PlayerService.getInstance();
	}

	public void tryExecute(AbstractAction action, Player player) {
		if (action.isAvailable(player)) {
			this.execute(action, player);
			action.refreshUserInterval(player);
		}else {
			this.getLogger().info("PlayerID={} can't attack yet.");
		}
	}

	protected abstract void execute(AbstractAction action, Player player);
	protected abstract Logger getLogger();

}
