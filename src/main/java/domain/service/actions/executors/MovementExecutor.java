package domain.service.actions.executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import domain.player.Player;
import domain.service.actions.AbstractAction;
import domain.service.actions.Movement;
import domain.utils.Position;

public class MovementExecutor extends AbstractActionExecutor {

	private final Logger LOGGER = LoggerFactory.getLogger(MovementExecutor.class);
	private static MovementExecutor instance;

	public static MovementExecutor getInstance() {
		if (instance == null) {
			instance = new MovementExecutor();
		}
		return instance;
	}

	@Override
	protected void execute(AbstractAction action, Player player) {
		Movement movement = (Movement) action;
		LOGGER.info("Turn playerID={} direction={}", player.id, movement.direction.name());
		player.direction = movement.direction;

		Position desiredPosition = player.nextTo(player.direction);
		LOGGER.info("Dsired position={}", desiredPosition.toString());
		Position oldPosition = new Position(player.position);
		LOGGER.info("Desired: {}", desiredPosition.toString());
		if (this.mapService.isEnabled(desiredPosition)) {
			player.move(desiredPosition);
			this.mapService.enablePosition(oldPosition);
			this.mapService.disablePosition(desiredPosition);
		} else {
			LOGGER.info("PlayerID={} -> Position disabled.", player.id);
		}
	}

	@Override
	protected Logger getLogger() {
		return LOGGER;
	}
}
