package domain.service.actions.executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import domain.player.Player;
import domain.service.actions.AbstractAction;
import domain.service.actions.RangeAttack;
import domain.utils.Position;

public class AttackExecutor extends AbstractActionExecutor {

	private final Logger LOGGER = LoggerFactory.getLogger(AttackExecutor.class);
	private static AttackExecutor instance;

	public static AttackExecutor getInstance() {
		if (instance == null) {
			instance = new AttackExecutor();
		}
		return instance;
	}

	@Override
	protected void execute(AbstractAction action, Player player) {
		Player target = this.findTarget(action, player);
		if (target != null) {
			LOGGER.info("ID={} golpea por {} a ID={}.", player.id, player.damage, target.id);
			target.receiveAttack(player.damage);
		}
	}

	private Player findTarget(AbstractAction action, Player player) {
		Position targetPosition = null;
		switch (action.getType()) {
		case MELEE_ATTACK:
			targetPosition = player.nextTo(player.direction);
			break;
		case RANGE_ATTACK:
			RangeAttack attack = (RangeAttack) action;
			targetPosition = new Position(attack.targetX, attack.targetY);
			break;
		default:
			throw new RuntimeException("Can't process action=" + action.getType());
		}
		return this.playerService.getPlayer(targetPosition);
	}

	@Override
	protected Logger getLogger() {
		return this.LOGGER;
	}

}
