package domain.service;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.esotericsoftware.kryonet.Connection;
import com.google.common.collect.Maps;

import domain.builder.PlayerBuilder;
import domain.player.Player;
import domain.utils.Position;

public class PlayerService {

	private static PlayerService instance ;
	private static final Logger LOGGER = LoggerFactory.getLogger(PlayerService.class);

	public static PlayerService getInstance() {
		if (instance == null) {
			instance = new PlayerService();
		}
		return instance;
	}

	private MapService mapService = MapService.getInstance();

	private Map<Connection, Player> players = Maps.newHashMap();

	public int newPlayer(Connection connection) {
		LOGGER.info("Creationg a new player.");
		Player player = PlayerBuilder.defaultPlayer();
		this.players.put(connection, player);
		this.mapService.disablePosition(player.position);
		return player.id;
	}

	public Map<Connection, Player> getPlayers() {
		return players;
	}

	public Player getPlayer(Connection connection) {
		return this.players.get(connection);
	}

	public Player getPlayer(Position position) {
		return this.players.values().stream().filter(p -> p.position.equals(position)).findFirst().orElse(null);
	}
}
