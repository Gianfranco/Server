package domain.service;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Maps;

import domain.utils.Position;

public class MapService {

	private static MapService instance;
	private static final Logger LOGGER = LoggerFactory.getLogger(MapService.class);

	public static MapService getInstance() {
		if (instance == null) {
			instance = new MapService();
		}
		return instance;
	}

	public static final int MAX_X = 20;
	public static final int MAX_Y = 20;

	private Map<Position, Boolean> map = Maps.newHashMap();

	private MapService() {
		LOGGER.info("Creating map.");
		for (int x = 0; x < MAX_X; x++) {
			for (int y = 0; y < MAX_Y; y++) {
				this.map.put(new Position(x, y), false);
			}
		}
		this.loadMap();
	}

	private void loadMap() {
		LOGGER.info("Putting initail map objects.");
		this.map.put(new Position(8, 8), true);
		
		for(int i =0 ; i<20; i++) {
			this.map.put(new Position(i, 19), true);
			this.map.put(new Position(i, 0), true);
			this.map.put(new Position(19, i), true);
			this.map.put(new Position(0, i), true);
		}
	}

	public void disablePosition(Position position) {
		LOGGER.info("Disabling positoin {}", position.toString());
		this.map.put(position, true);
	}

	public boolean isEnabled(Position position) {
		return !this.isDisabled(position);
	}

	public boolean isDisabled(Position position) {
		return this.map.get(position);
	}

	public void enablePosition(Position position) {
		this.map.put(position, false);
	}
}
