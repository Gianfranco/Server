package domain.enums;

import com.google.common.collect.Lists;

public enum DirectionEnum {
    UP(1), DOWN(2), RIGHT(3), LEFT(4);

    private Integer index;

    DirectionEnum(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public static DirectionEnum parse(Integer index) {
        return Lists.newArrayList(DirectionEnum.values())
                .stream()
                .filter(directionEnum -> directionEnum.index.equals(index))
                .findFirst()
                .get();
    }
}
