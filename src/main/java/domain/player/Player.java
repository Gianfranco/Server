package domain.player;

import java.util.Date;

import domain.enums.DirectionEnum;
import domain.utils.Position;

public class Player {

	public int id;
	public Position position;
	public int minHp;
	public int maxHp;
	public int damage;
	public DirectionEnum direction;
	public long lastMovementTime;
	public long lastAttackTime;

	public void move(Position position) {
		this.position = position;
	}

	public boolean isDead() {
		return minHp == 0;
	}

	public void receiveAttack(int damage) {
		this.minHp = damage >= this.minHp ? 0 : this.minHp - damage;
	}

	public void updateLastMovementTime() {
		this.lastMovementTime = new Date().getTime();
	}

	public void updateLastAttack() {
		this.lastAttackTime = new Date().getTime();
	}
	
	public Position nextTo(DirectionEnum direction) {
		return this.position.nextTo(direction);
	}

}
