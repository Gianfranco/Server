package domain.builder;

import static domain.enums.DirectionEnum.DOWN;

import domain.player.Player;
import domain.utils.Position;

public class PlayerBuilder {

	private static int maxId = 0;

	public static Player defaultPlayer() {
		Player player = newPlayer();
		Position position = new Position(5, 5);
		player.position = position;
		player.minHp = 150;
		player.maxHp = 150;
		player.damage = 50;
		return player;
	}

	private static Player newPlayer() {
		Player player = new Player();
		player.direction = DOWN;
		player.id = maxId;
		maxId++;
		return player;
	}
}
