package controller.sender;

import com.esotericsoftware.kryonet.Connection;
import controller.dtos.outputs.PlayerDTO;
import domain.player.Player;
import domain.service.PlayerService;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import controller.dtos.outputs.OutputDTO;

public class Sender implements Runnable {

	private PlayerService playerService = PlayerService.getInstance();

	@Override
	public void run() {
		while (true) {
			Map<Connection, Player> players = this.playerService.getPlayers();
			Collection<Player> onlyPlayers = players.values();
			players.forEach((connection, player) -> send(connection, player, onlyPlayers));
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void send(Connection connection, Player player, Collection<Player> players) {
		OutputDTO output = createOutputDTO(player, players);
		connection.sendUDP(output);
	}

	private OutputDTO createOutputDTO(Player player, Collection<Player> players) {
		OutputDTO outputDTO = new OutputDTO();
		outputDTO.players = createPlayerDTOs(players);
		return outputDTO;
	}

	private List<PlayerDTO> createPlayerDTOs(Collection<Player> players) {
		return players.stream().map(PlayerDTO::new).collect(Collectors.toList());
	}

}
