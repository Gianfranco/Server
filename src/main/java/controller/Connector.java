package controller;

import java.io.IOException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;

import controller.dtos.inputs.ActionDTO;
import controller.dtos.outputs.OutputDTO;
import controller.dtos.outputs.PlayerDTO;
import controller.exceptions.UnableStartServerException;
import controller.mappers.ActionDTOMapper;
import domain.player.Player;
import domain.service.PlayerService;
import domain.service.actions.AbstractAction;
import domain.service.actions.ActionManager;

public class Connector {

	private static final Logger LOGGER = LoggerFactory.getLogger(Connector.class);
	private static final int TCP_PORT = 4545;
	private static final int UDP_PORT = 5454;

	private Server server = new Server();
	private PlayerService playerService = PlayerService.getInstance();
	private ActionManager actionService = ActionManager.getInstance();

	public Connector() {
		LOGGER.info("Creating connector.");
		this.server.start();
		this.configureServer();
		this.addListener();
	}

	private void configureServer() {
		LOGGER.info("Loading server settings.");
		try {
			this.server.bind(TCP_PORT, UDP_PORT);
			this.configureOutputTypes();
		} catch (IOException e) {
			throw new UnableStartServerException("Error trying start up the server.");
		}
	}

	private void addListener() {
		LOGGER.info("Listening.");
		this.server.addListener(new Listener() {
			public void received(Connection connection, Object object) {

				Player player = playerService.getPlayer(connection);

				if (object instanceof AbstractAction) {
					actionService.process((AbstractAction) object, player);
				}

				if (object instanceof String) {
					Integer id = playerService.newPlayer(connection);
					LOGGER.info("New player has been logged, setting id={}", id);
					connection.sendTCP(id);
				}

				if (object instanceof ActionDTO) {
					LOGGER.info("Action received from playerID={}", player.id);
					actionService.process(ActionDTOMapper.map((ActionDTO) object), player);
				}
			}
		});
	}

	private void configureOutputTypes() {
		LOGGER.info("Registering output objects.");
		this.server.getKryo().register(ActionDTO.class);
		this.server.getKryo().register(OutputDTO.class);
		this.server.getKryo().register(PlayerDTO.class);
		this.server.getKryo().register(ArrayList.class);
	}

}
