package controller.mappers;

import controller.dtos.inputs.ActionDTO;
import domain.enums.DirectionEnum;
import domain.service.actions.AbstractAction;
import domain.service.actions.MeleeAttack;
import domain.service.actions.Movement;
import domain.service.actions.RangeAttack;

public class ActionDTOMapper {

	public static AbstractAction map(ActionDTO dto) {
		switch (dto.type) {
		case 1:
			return new Movement(DirectionEnum.parse(dto.direction));
		case 2:
			return new MeleeAttack();
		case 3:
			return new RangeAttack(dto.targetX, dto.targetY);
		default:
			throw new RuntimeException("Unknow action index=" + dto.type);
		}
	}
}
