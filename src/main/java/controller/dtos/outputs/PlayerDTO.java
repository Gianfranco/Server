package controller.dtos.outputs;

import domain.player.Player;

public class PlayerDTO {

	public int id;
	public int minHp;
	public int maxHp;
	public int x;
	public int y;
	public int direction;

	public PlayerDTO(Player player) {
		this.id = player.id;
		this.minHp = player.minHp;
		this.maxHp = player.maxHp;
		this.x = player.position.x;
		this.y = player.position.y;
		this.direction = player.direction.getIndex();
	}
}
