package controller.exceptions;

public class UnableStartServerException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public UnableStartServerException(String msg) {
		super(msg);
	}

}
