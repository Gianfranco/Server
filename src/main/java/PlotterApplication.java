import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import controller.Connector;
import controller.sender.Sender;

public class PlotterApplication {

	private final static Logger LOGGER = LoggerFactory.getLogger(PlotterApplication.class);

	public static void main(String[] args) {
		LOGGER.info("Starting applicattion.");
		Thread t = new Thread(new Sender());
		t.start();
		new Connector();
	}
}
